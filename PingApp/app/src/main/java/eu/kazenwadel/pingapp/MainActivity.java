package eu.kazenwadel.pingapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ClipboardManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {



    SharedPreferences sharedPreferences;
    public ArrayAdapter<String> adapter;
    String ipkey  = "eu.kazenwadel.app.ipkey";

    //static final String [] ips =new String[]{"100.118.3.4","100.118.3.5","100.118.3.5"};
    public static List<String> ips = new ArrayList<>();
    public static List<String> names = new ArrayList<>();
    public static List<Boolean> reachable = new ArrayList<>();
    public static List<String> stringList=new ArrayList<>();


    //shared prefs are saved by: IP,name;IP,name;IP,name

    // split shared preferences
    void loadPrefs(String prefs){
        ips.clear();
        names.clear();
        while (prefs.toLowerCase().contains(":")){
            Log.d("Remaining",prefs);

            String [] parts = prefs.split(":", 2);

            prefs=parts[1];
            Log.d("Split1",parts[1]);
            splitipandname(parts[0]);

        }

        Log.d("Last",prefs);

        splitipandname(prefs);
    }

    //split name and ip combination
    void splitipandname(String combi){
        if (combi.contains(",")){
            String [] nameorip = combi.split(",",2);
            ips.add(nameorip[0]);
            Log.d("SP1",nameorip[0]);
            names.add(nameorip[1]);
            Log.d("SP2",nameorip[1]);
        }

    }

    // write sharedprefs
    void saveSharedPrefs(){
        int index=0;
        String tempPref="";
        for(String ip:ips){
            tempPref=tempPref+":"+ip+","+names.get(index);
            index++;
        }
        sharedPreferences.edit().putString(ipkey, tempPref).apply();
    }

    public boolean pingIP(String testIp) throws MalformedURLException {
        String command = "/system/bin/ping -c 1 " + testIp;
        Runtime runtime = Runtime.getRuntime();
        try {
            Log.d("PING",command);

            Process ipProcess = runtime.exec(command);
            int exitValue = ipProcess.waitFor();
            Log.d("PING","Exitvalue: "+exitValue);
            return (exitValue == 0);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    final public void updateAdapter() {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        adapter.notifyDataSetChanged();
                    }
                }, 3000);   //3 seconds

            }
        });
    }

    public void refreshList(){
        int shortindex=0;
        for (String ip:ips){
            reachable.set(shortindex,false);
            shortindex++;
        }
        adapter.notifyDataSetChanged();

        int index=0;
        for (final String ip:ips){
            final int finalIndex = index;
            new Thread(new Runnable() {
                public void run(){
                    try {
                        //reachable.set(finalIndex,false);
                        if (pingIP(ip)){
                            reachable.set(finalIndex,true);
                        }
                        else{
                            reachable.set(finalIndex,false);
                        }
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                }
            }).start();

            index++;
        }
        //reachable.set(1,false);
        //reachable.set(0,true);
        updateAdapter();
    }

    void buildstringlist(){
        reachable.clear();
        stringList.clear();
        for (String ip:ips){
            reachable.add(false);
            Log.d("IP",ip);
        }
        int index=0;
        for (String ip:ips){
            stringList.add(ip+" | "+names.get(index));
            Log.d("Add",ip+" | "+names.get(index));
            index++;
        }
    }

    void openContentPopup(final int position, View view){
        LayoutInflater inflater = (LayoutInflater)
                getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.popup_window, null);

        // create the popup window
        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        boolean focusable = true; // lets taps outside the popup also dismiss it
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);
        // show the popup window
        // which view you pass in doesn't matter, it is only used for the window tolken

        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

        final EditText popupName= popupView.findViewById(R.id.popupName);
        final EditText popupIP =popupView.findViewById(R.id.popupIP);

        if (position>ips.size()){
            popupName.setText("Name");
            popupIP.setText("IP");
        }else{
            popupName.setText(names.get(position));
            popupIP.setText(ips.get(position));
        }



        Button save = popupView.findViewById(R.id.save);
        Button delete = popupView.findViewById(R.id.delete);
        if(position>ips.size()){
            delete.setVisibility(view.INVISIBLE);
        }
        else{
            delete.setVisibility(view.VISIBLE);
        }
        Button exit= popupView.findViewById(R.id.exit);

        save.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                if (position>ips.size()){
                    ips.add(popupIP.getText().toString());
                    names.add(popupName.getText().toString());
                    reachable.add(false);
                }else{
                    ips.set(position,popupIP.getText().toString());
                    names.set(position,popupName.getText().toString());
                    reachable.set(position,false);

                }
                saveSharedPrefs();
                buildstringlist();
                adapter.notifyDataSetChanged();

                popupWindow.dismiss();
            }
        });

        exit.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                popupWindow.dismiss();
            }
        });

        delete.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                ips.remove(position);
                names.remove(position);
                reachable.remove(position);
                saveSharedPrefs();
                buildstringlist();
                adapter.notifyDataSetChanged();
                popupWindow.dismiss();
            }
        });
    }

    void openExportPopup(View view){
        LayoutInflater inflater = (LayoutInflater)
                getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.popup_exp_imp, null);

        // create the popup window
        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        boolean focusable = true; // lets taps outside the popup also dismiss it
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);
        // show the popup window
        // which view you pass in doesn't matter, it is only used for the window tolken

        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);


        final EditText config= popupView.findViewById(R.id.config);
        config.setText(sharedPreferences.getString(ipkey, new String()));



        Button copy = popupView.findViewById(R.id.copy);
        Button paste = popupView.findViewById(R.id.paste);

        copy.setOnClickListener(new View.OnClickListener() { // set onclick listener to my textview
            @Override
            public void onClick(View view) {
                ClipboardManager cm = (ClipboardManager)getApplicationContext().getSystemService(Context.CLIPBOARD_SERVICE);
                cm.setText(config.getText().toString());
                Toast.makeText(getApplicationContext(), "Copied :)", Toast.LENGTH_SHORT).show();
            }
        });

        paste.setOnClickListener(new View.OnClickListener() { // set onclick listener to my textview
            @Override
            public void onClick(View view) {
                ClipboardManager cm = (ClipboardManager)getApplicationContext().getSystemService(Context.CLIPBOARD_SERVICE);
                //cm.setText(config.getText().toString());
                config.setText(cm.getText());
                Toast.makeText(getApplicationContext(), "Pasted :)", Toast.LENGTH_SHORT).show();
            }
        });


        Button save = popupView.findViewById(R.id.save);
        Button exit= popupView.findViewById(R.id.exit);

        save.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //sharedPreferences.edit().clear().commit();
                //sharedPreferences.edit().putString(ipkey, config.getText().toString()).apply();
                String configText=config.getText().toString();
                if(configText != null) {
                    Log.d("Export",configText);
                    loadPrefs(configText);
                }

                saveSharedPrefs();
                buildstringlist();

                adapter.notifyDataSetChanged();
                popupWindow.dismiss();
            }
        });

        exit.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });
    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        sharedPreferences = this.getSharedPreferences(
                "eu.kazenwadel.app", Context.MODE_PRIVATE);


        //sharedPreferences.edit().clear().commit();
        //sharedPreferences.edit().putString(ipkey, "8.8.8.8,Google;8.8.8.8.8,Google2").apply();
        //sharedPreferences.edit().putString(ipkey, "Google,8.8.8.8").apply();

        String shPref = sharedPreferences.getString(ipkey, new String());

        if(shPref != null && !shPref.isEmpty()) {
            loadPrefs(shPref);
        }

        Log.d("TEst", shPref);

        buildstringlist();

        adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, stringList) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);

                TextView textView = (TextView) view.findViewById(android.R.id.text1);

                if (reachable.get(position)){
                    textView.setBackgroundColor(Color.WHITE);
                }else{
                    textView.setBackgroundColor(Color.RED);
                }
                Log.d("Adapter","set");
                return view;
            }
        };

        //refreshList();

        final ListView listView= findViewById(R.id.listView);
        listView.setAdapter(adapter);



        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                //Object o = prestListView.getItemAtPosition(position);
                Log.d("OnClidk",Integer.toString(position));
                // inflate the layout of the popup window
                openContentPopup(position, view);




                // dismiss the popup window when touched

            }
        });




        FloatingActionButton fab= findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view , "Pinging... Wait 3 Seconds", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                refreshList();
            }
        });

        FloatingActionButton fab_new= findViewById(R.id.fab_new);
        fab_new.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                openContentPopup(ips.size()+1,view);
            }
        });

        FloatingActionButton fab_export= findViewById(R.id.fab_export);
        fab_export.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                openExportPopup(view);
            }
        });
    }


}

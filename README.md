# Ping Server

A simple Android App that checks for the status of servers.

You can add Servers, delete them, ping them, or share/import the configuration
if you work with other people on the same network.

Sorry for the missing comments and docu, this was a one-day build.

<p align="center"><img src="/Screenshots/screenshot.jpg" width="350"/></p>
